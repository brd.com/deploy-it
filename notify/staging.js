const E = process.env;
const JIRA_VERSION_LINK_TEMPLATE = process.env.JIRA_VERSION_LINK_TEMPLATE || `https://breadwallet.atlassian.net/issues/?jql=fixVersion%20%3D%20__VERSION__`;

const m = E.VERSION.match(/^(\d+)(\.(\d+))?(\.(\d+))?/);
const jiraVersion = m && m[0];

hash = {
  text: `Deployed ${E.VERSION} of ${E.CI_PROJECT_NAME} to staging.`,
  attachments: [{
    fallback: `Unable to link to JIRA`,
    actions: []
  }]
};

if(jiraVersion) {
  const VERSION=encodeURIComponent(jiraVersion);
  
  hash.attachments[0].actions.push({
    type: 'button',
    text: `${jiraVersion} in JIRA`,
    url: JIRA_VERSION_LINK_TEMPLATE.replace(/__VERSION__/g,VERSION),
  });
}

try {
  const child_process = require('child_process');
  const mergeRequestIID = Number(child_process.execSync(`git ls-remote --refs origin refs/merge-requests/*/head | grep $(git rev-parse HEAD) | awk -F / '{print$3}'`,{shell:'/bin/bash'}).toString('utf-8'));
  
  if(mergeRequestIID) {
    hash.attachments[0].actions.push({
      type: 'button',
      text: `MR in GitLab`,
      url: `${E.CI_PROJECT_URL}/merge_requests/${mergeRequestIID}`
    });
  }
} catch(x) {
  console.error("Error getting MR identifier.");
  console.error(x);
}

if(E.CI_ENVIRONMENT_URL) {
  hash.attachments[0].actions.push({
    type: 'button',
    text: 'Open in Browser',
    url: E.CI_ENVIRONMENT_URL
  });
}

module.exports = hash;
