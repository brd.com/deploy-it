#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const unet = require('unet');

const unique = (array) => array.filter((item,index,array) => array.indexOf(item) === index);

function notifyTmpl(path) {
  if(path.match(/\.json$/)) {
    var text = fs.readFileSync(path).toString('utf-8');
    text = text.replace(/(?:\${(.*?)})|(?:\$([A-Z_]*))/g,function(match,a,b) {
      return process.env[a||b] || '';
    });
    return JSON.parse(text);
  } else {
    return require(path);
  }
}

(async function() {
  var hash;

  console.log("Trying env ",`${process.argv[2].toUpperCase()}_NOTIFY`);
  var options = [
    process.argv[2],
    process.env[`${process.argv[2].toUpperCase()}_NOTIFY`],
    path.resolve(process.cwd(),`./notify/${process.argv[2]}.js`),
    path.resolve(process.cwd(),`./notify/${process.argv[2]}.json`),
    path.resolve(__dirname,`./notify/${process.argv[2]}.js`),
    path.resolve(__dirname,`./notify/${process.argv[2]}.json`)
  ];

  console.log("PRE-options: ",options);
  options = unique(options.filter(f => fs.existsSync(f)));
  console.log("options: ",options);

  if(options.length == 0) {
    throw new Error(`No templates specified, add one in notify/${process.argv[2]}.js`);
  }
  
  hash = notifyTmpl(options[0]);
  
  const res = await unet({
    url: process.env.SLACK_WEBHOOK_URL,
    method: 'POST',
    body: hash
  });
  
  console.log(`STATUS: ${res.response.status}`);
})();
