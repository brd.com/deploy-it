# @brd.com/deploy-it

BRD's node deployment tools.  In simple terms, this module has some
sensible defaults for deploying plain node apps and nuxt apps.  These
get put together in the `gitlab-functions`, and can be overridden by
javascript files in the `deploy` directory.  Any files in the `deploy`
directory get executed, and whatever is exported by `module.exports`
gets written in JSON and sent to kubectl for deployment.

## General usage

Installing deploy-it globally (`npm i -g @brd.com/deploy-it`) gives
you a global executable (`release-it`) that you can use to release
BRD-style repositories.  Check out the code for details on how it
works, but it should kinda walk you through the process as necessary.

```
npm i -g @brd.com/deploy-it
release-it 5.0.4
```

## Repo configuration

You can override the JSON whole-cloth by exporting an object, or you
can export a function which takes the default config and modify it to
suit your needs.

Here is how you would set additional environment variables for an
application:

```js
// in deploy/deployment.js (in your repo, not this one)
module.exports = function(config) {
  let env = config.spec.template.spec.containers[0].env;

  env.push({
    name: 'RELEASED_AT',
    value: (new Date()).toISOString(),
  });

  env.push({
    name: 'WYRE_STAGING_API_SECRET',
    valueFrom: {
      secretKeyRef: {
        name: 'staging-wyre',
        key: 'api-secret',
      }
    }
  });

  return config;
}
```

Here's how you'd change the secret name for the ingress:

```js
// in deploy/ingress.js (in your repo, not this one)

const presets = {
  production: 'foo-production-tls',
  staging: 'foo-staging-tls',
};

module.exports = function(config) {
  config.spec.tls[0].secretName = presets[process.env.CI_ENVIRONMENT_SLUG] || 'wildcard-partners-tls';
  return config;
}
```

But what do you do if gitlab is all full o' runners? You'll need to have owner access and an access token with API scope:

```bash
export GITLAB_ACCESS_TOKEN = 'foo' # set this with an API scope
node # start a REPL
const g = require('gitlab-tools.js'); // import the tools in this package
g.deleteRunnersRecursive('runners\-.+\-normal') // or something like this
```
