const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || '';

function slugify(string) {
 return string && string.replace(/[^-A-Za-z0-9_.]+/g,'-').replace(/^[-0-9_.]*/,'').replace(/[-_.]*$/,'');
}

module.exports = {
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "labels": {
      "app": ENV_SLUG,
      "ci-environment-slug": ENV_SLUG,
    },
    "name": ENV_SLUG,
  },
  "spec": {
    "replicas": Number(process.env.REPLICAS) || 1,
    "selector": {
      "matchLabels": {
        "app": ENV_SLUG
      }
    },
    "template": {
      "metadata": {
        "labels": {
          "app": ENV_SLUG,
          "git-ref": slugify(process.env.CI_COMMIT_REF_NAME),
          "git-sha": process.env.CI_COMMIT_SHA,
          "git-short-sha": process.env.CI_COMMIT_SHORT_SHA,
          "git-branch": slugify(process.env.CI_COMMIT_BRANCH),
          "git-tag": slugify(process.env.CI_COMMIT_TAG),
        }
      },
      "spec": {
        "containers": [
          {
            "name": "app",
            "readinessProbe": {
              "tcpSocket": {
                "port": 8080
              },
              "initialDelaySeconds": 2,
              "periodSeconds": 10
            },
            "command": [
              "bash",
              "-c",
              "npm run start"
            ],
            "env": [
              {
                "name": "PORT",
                "value": "8080"
              },
              {
                "name": "HOST",
                "value": "0.0.0.0"
              },
              {
                "name": "NODE_ENV",
                "value": `${process.env.NODE_ENV}`,
              },
              {
                "name": "GCLOUD_PROJECT_ID",
                "value": `${process.env.GCLOUD_PROJECT_ID}`,
              },
              {
                "name": "DOCKER_ENV",
                "value": "kubernetes"
              },
              {
                "name": "BASE_URL",
                "value": `https://${process.env.APP_HOST}${process.env.APP_PATH}`,
              },
              {
                "name": "APP_HOST",
                "value": `${process.env.APP_HOST}`,
              },
              {
                "name": "APP_PATH",
                "value": `${process.env.APP_PATH}`,
              },
              {
                "name": "SENTRY_DSN",
                "value": `${process.env.SENTRY_DSN}`,
              },
              {
                "name": "SENTRY_ENV",
                "value": `${ENV_SLUG}`,
              },
              {
                "name": "SENTRY_VERSION",
                "value": `${process.env.VERSION}`,
              },
              {
                "name": "BRD_API",
                "value": `${process.env.BRD_API || 'https://stage2.breadwallet.com'}`,
              },
              {
                "name": "COOKIE_SECRET",
                "valueFrom": {
                  "secretKeyRef": {
                    "name": `${ENV_SLUG}-keys`,
                    "key": "cookie-secret"
                  }
                }
              },
            ],
            "image": process.env.IMAGE,
            "imagePullPolicy": "IfNotPresent",
            "ports": [
              {
                "containerPort": 8080,
                "name": "app",
                "protocol": "TCP"
              }
            ]
          }
        ],
        "restartPolicy": "Always"
      }
    }
  }
}
