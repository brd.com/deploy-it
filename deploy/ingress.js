const fs = require('fs');

const tlsSecret = {
  'deploy-production': 'production-tls',
  'deploy-staging': 'staging-tls',
  'deploy-review': 'review-wildcard-tls',
};

let overallAnnotations = {};

if(process.env.DI_INGRESS_ANNOTATIONS) {
  overallAnnotations = JSON.parse(fs.readFileSync(process.env.DI_INGRESS_ANNOTATIONS,'utf-8'));
}

console.log("Overall annotations: ",overallAnnotations);

let hash = {
  "apiVersion": "networking.k8s.io/v1beta1",
  "kind": "Ingress",
  "metadata": {
    "annotations": Object.assign({
      "kubernetes.io/ingress.class": "nginx"
    },overallAnnotations),
    "generation": 4,
    "labels": {
      "app": `${process.env.CI_ENVIRONMENT_SLUG}`,
      "ci-environment-slug": `${process.env.CI_ENVIRONMENT_SLUG}`,
    },
    "name": `${process.env.CI_ENVIRONMENT_SLUG}`,
  },
  "spec": {
    "rules": [
      {
        "host": `${process.env.APP_HOST}`,
        "http": {
          "paths": [
            {
              "backend": {
                "serviceName": `${process.env.CI_ENVIRONMENT_SLUG}`,
                "servicePort": 8080
              },
              "path": `${process.env.APP_PATH}`
            }
          ]
        }
      }
    ],
    "tls": [
      {
        "hosts": [
          `${process.env.APP_HOST}`
        ],
        "secretName": tlsSecret[process.env.CI_JOB_STAGE] || 'review-wildcard-tls'
      }
    ]
  }
}


try {
  const [m,stage] = process.env.CI_JOB_STAGE.match(/^deploy-(.*?)$/) || [];
  const env = `DI_${stage.replace(/-/g,'_').toUpperCase()}_INGRESS_ANNOTATIONS`
  const arg = process.env[env];

  if(arg && fs.existsSync(arg)) {
    console.log(`Applying ingress annotations from ${env}:`);
    const  annotations = JSON.parse(fs.readFileSync(arg,'utf-8'));
    console.log(` => `,annotations);
    hash.metadata.annotations = Object.assign(hash.metadata.annotations,annotations);
  }
} catch(x) {
  console.error("Error applying annotations: ",x);
}

module.exports = hash;
