#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

var ourFiles = fs.readdirSync(path.resolve(__dirname,'deploy'));
var localPath = path.resolve(process.cwd(),'deploy');
var localFiles = fs.existsSync(localPath) && fs.readdirSync(localPath) || [];

const unique = (array) => array.filter((item,index,array) => array.indexOf(item) === index);

var files = unique(ourFiles.concat(localFiles));

(async function() {
  try { 
    for(let i = 0 ; i < files.length ; i++) {
      let file = files[i];
      
      let ourPath = path.resolve(__dirname,'deploy',file);
      let localPath = path.resolve(process.cwd(),'deploy',file);

      var stack = unique([ourPath,localPath]).filter(p => fs.existsSync(p));

      var out = await stack.reduce(async (result,path) => {
        result = await result;
        var res = require(path);
        
        if(typeof res == 'function') return await res(result);
        
        return res;
      },null);
      
      
      if(out) {
        var base = path.join(process.cwd(),process.argv[2] || '.deploy')
        if(!fs.existsSync(base)) {
          fs.mkdirSync(base);
        }
        
        let outFile = path.join(base,file.replace(/\.js$/,'.json'));
        
        fs.writeFileSync(outFile,JSON.stringify(out,null,2),{ encoding: 'utf-8' });
      } else {
        console.log(`Not writing ${file}`);
      }
    }
  } catch(x) {
    console.error("ERROR DURING TEMPLATING");
    console.error(x);

    process.exitCode = 1;
  }
})();
